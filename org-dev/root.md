---
id: C33IQbjVihnHgVU6
title: Root
desc: ''
updated: 1628124074812
created: 1628124074812
---
# Welcome to Dendron

This is the root of your dendron vault. If you decide to publish your entire vault, this will be your landing page. You are free to customize any part of this page except the frontmatter on top. 
